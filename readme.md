# 1. ABSTRACT
*  I’ve been working with Java as a back-end since 2009, and abstract classes and methods are pretty nice. How can I create an abstract method (method = function in a class) in my superclass that forces an implementation when extending it?

* There are no abstract methods implemented in Javascript so far and I am not using something like TypeScript, which I could do it. So a tip is to throw an Error in your super method because when you implement it, it simply overrides the method in the superclass.

# Table of Content

 1. Abstract 
 2. Introduction

     2.1 ECMAScript vs JavaScript

     2.2 ES6

     2.3 Arrow function

     2.4 Template Literals

 3. Object and Array Destructing

     3.1 Object Destruction

     3.2 Array Destruction

 4. Default Parameter

 5. COnclusion

 6. Reference

## 2. Introduction to ES6

* ES6 or ECMAScript 2015 is the 6th version of the ECMAScript programming language. ECMAScript is the standardization of Javascript which was released in 2015 and subsequently renamed ECMAScript 2015.
* ECMAScript and Javascript are both different.

## 2.1 ECMAScript vs Javascript

* ECMAScript: It is the specification defined in ECMA-262 for creating a general-purpose scripting language. In simple terms, it is a standardization for creating a scripting language. It was introduced by Ecma International and is an implementation with which we learn how to create a scripting language. 

* Javascript: A general-purpose scripting language that conforms to the ECMAScript specification. It is an implementation that tells us how to use a scripting language.

 ## 2.2 ES6

 * Javascript ES6 has been around for a few years now, and it allows us to write code in a clever way which makes the code more modern and more readable. It’s fair to say that with the use of ES6 features we write less and do more, hence the term ‘write less, do more’ definitely suits ES6. 

* ES6 introduced several key features like const, let arrow functions, template literals, default parameters, and a lot more. Let’s take a look at them one by one.

* “const” and “let” 

* Before ES6 we mainly made use of the var keyword whenever we wanted to declare a variable. But it had some serious issues, also it was not the developers’ favorite so in the ES6 version, we were introduced to const and let keywords which allows us to store variables. They both have their way of storing variables.
* const: The const keyword is mainly used to store that variable whose value is not going to be changed. Consider an example where you are creating a web application where you want to store a variable whose value is not going to change, then const is the best choice to store that variable. In javascript, const is considered to be more powerful than var. Once used to store a variable it can’t be reassigned. In simple words, it is an immutable variable except when used with objects.

## Example:
// Const

const name = 'Mukul';

console.log(name); 
// Will print 'Mukul' to the console.


// Trying to reassign a

const variable
name = 'Rahul';

console.log(name); 
// Will give TypeError.


- ![OUTPUT](https://media.geeksforgeeks.org/wp-content/uploads/20190225102537/Screenshot-1081-e1551070554179.png)

* In the above code we declared a variable name with const keyword and then console.log it which works fine, but reassigning it with some other value will give an error. Now let’s look at an example where we declare an object using the const keyword.

###  Example:

// Object using const

const person ={

 	name:'Mukul Latiyan',

 	age:'21',

 	isPlaced:true

 };

console.log(person.name); 
// 'Mukul Latiyan'

person.name = 'Mayank';

console.log(person.name); 
//'Mayank'

+ ![Output](https://media.geeksforgeeks.org/wp-content/uploads/20190225102852/Screenshot-1092-e1551070763607.png)

## 2.3 Arrow functions

- Arrow functions(also known as ‘fat arrow functions’) are a more concise syntax for writing function expressions. Introduced in ES6, arrow functions are one of the most impactful changes in javascript. These function expressions make your code more readable, and more modern. 

### Example:

// ES5

function printName(name){

    console.log('Hello '+name);

 }

 printName('Mukul'); 
 // Hello Mukul


-  ![Output](https://media.geeksforgeeks.org/wp-content/uploads/20190225104013/Screenshot-1121-e1551071431449.png)

*  Instead of using this, use the below code: 

 // ES6

 const printName = name =>{

 	return `Hi ${name}`; // using template literals

}

 console.log(printName('Mukul')); // Hi Mukul

 // We can also write the above code without

 // the return statement

 const printName1 = name => `Hi ${name}`;

 console.log(printName1('Mukul')); // Hi Mukul

-  ![Output](https://media.geeksforgeeks.org/wp-content/uploads/20190225104218/Screenshot-1131-e1551071690842.png)

## 2.4 Template literal

* Template literals are a feature of ES6 which allows us to work with strings in a better way than when compared to ES5 and below. By simply using template literals, we can improve the code readability. Before ES6 we made use of the ‘+’ operator whenever we wanted to concatenate strings and also when we wanted to use a variable inside a string which is not a recommended method. Template literals use back-ticks(“) rather than the single or double quotes we’ve used with regular strings.

### Example

 // ES5

 var name = 'Mukul Latiyan';

 console.log('My name is '+ name);

 // ES6

 const name1 = 'Mukul Latiyan';

 console.log(`My name is ${name1}`);

 ![Output](https://media.geeksforgeeks.org/wp-content/uploads/20190225104846/Screenshot-114-e1551071950696.png)

## 3. Object and Array Destructing

* Destructing in javascript means the breaking down of a complex structure(Objects or arrays) into simpler parts. With the destructing assignment, we can ‘unpack’ array objects into a bunch of variables.

## 3.1 Object destruction

 // ES5

 const college = {

 	name : 'DTU',

	established : '1941',

	isPrivate : false

 };


 let name = college.name;

 let established = college.established;

 let isPrivate = college.isPrivate;

 console.log(name);
  // DTU
 console.log(established); 
 // 1941

 console.log(isPrivate);
  // false

- ![Output](https://media.geeksforgeeks.org/wp-content/uploads/20190225105039/Screenshot-1151-e1551072867987.png)

### The new way to do this using ES6 is: 

 // ES6

 const college = {

	name : 'DTU',
	established : '1941',
	isPrivate : false
 };

 let{
    name,established,
    isPrivate
    } = college;

 console.log(name); 
 // DTU
 console.log(established); 
 // 1941
 console.log(isPrivate);
  // false

- ![Output](https://media.geeksforgeeks.org/wp-content/uploads/20190225105039/Screenshot-1151-e1551072867987.png)

## 3.2 Array destruction

 In case of destruction of arrays we can simply replace the curly brackets with square brackets. 

#### Like:

 // ES6

 const arr = ['lionel','messi','barcelona'];

 let[value1,value2,value3] = arr;

 console.log(value1); 
 // lionel
 console.log(value2); 
 // messi
 console.log(value3); 
 // barcelona

- ![Output](https://media.geeksforgeeks.org/wp-content/uploads/20190225105254/Screenshot-116-e1551072189159.png)

## 4. Default Parameters

 * Default parameters allow you to define a parameter in advance, which can be helpful in certain scenarios. In javascript, the function parameter defaults to undefined. However, it is useful to set a different default value. Before ES6 the way we used to define default parameters is by testing parameters' values in the default function body and assigning a value if they are undefined. 

### Example:
 // ES5

 function fun(a,b){

	return a+b;
 }

 console.log(fun(2,1));
  // 3
 console.log(fun(2));
  // 2 + undefined is NaN(not-a-number)

- ![Output](https://media.geeksforgeeks.org/wp-content/uploads/20190225105427/Screenshot-1171-e1551072290560.png)

*  We removed this default parameter error like this: 
 // ES5(improved)

 function fun(a,b){

	b = (typeof b!=='undefined')?b:1;
	return a + b;
 }

 console.log(fun(2,1)); 
 // 3
 console.log(fun(2));
// 3

*  ![Output](https://media.geeksforgeeks.org/wp-content/uploads/20190225110630/Screenshot-1191-e1551073004362.png)

## 5. Conclusion

* Javascript ES6 transformed not only the way developers use Javascript but the update processes of the Javascript language as well. Thanks to Javascript ES6, new features enter the Javascript language faster than ever before and Javascript can keep up with the changing modern web.

* In this article, we covered many of the new features, syntax, and upgrades from ES5 to ES6. This includes new variable types let and const, classes, template literals, new String and Array methods, new Object shorthand syntax, arrow functions, rest and spread syntax, destructuring, promises, and modules. With this reference, you should be able to read, write, and understand the syntax of the next generation of JavaScript.

## 6. References

* https://www.udemy.com/course/javascript-es6-a-complete-reference-guide-to-javascript-es6/

* https://www.tutorialspoint.com/es6/es6_quick_guide.htm

* https://www.javascripttutorial.net/es6/g