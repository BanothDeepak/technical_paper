Node Version Manager (NVM)
It is a tool used to manage multiple active Node.js versions.

The Node.js platform, Node.js ​community of tools, and Node.js libraries are fast-moving targets – what might work under one Node.js version is not guaranteed to work for another version of Node.js. Hence, users need ways to switch between multiple versions of Node.js

Why use NVM?
NVM allows users to:

Locally download any of the remote Long Term Support (LTS) versions of Node.js with a simple command.
Easily switch between multiple versions of Node.js, right from the command line.
Set up aliases to switch between different downloaded versions of Node.js with ease.
Learn more about nvm here.

For Linux and Mac users only, Windows users can check the last section
1. Installing NVM
Run the NVM installer using either of the following commands:

curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash
or

wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash
2. Verify Installation
To verify that nvm has been installed, do:

command -v nvm
which should output nvm if the installation was successful. Please note that which nvm will not work, since nvm is a sourced shell function, not an executable binary.

3. Using NVM
Once NVM is installed it allows users to install any version of Node.js through a simple command:

nvm install <SPECIFIC_NODE_VERSION>
Recommended: To install the latest LTS (Long Term Support) version of Node.js, use the following command:

nvm install --lts
To use any specific version of Node.js for your code, use the following command:

nvm use <SPECIFIC_NODE_VERSION>